#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "rbtree.h"

#define assert(cond) do { \
    if (!(cond)) { \
        printf("assert fail: %s, line %d\n", #cond, __LINE__); \
        exit(1); \
    } \
} while (0)

struct mynode {
    struct tree_head n;
    int value;
};

void print(struct mynode *node) {
    if (node->n.left != NIL) print((struct mynode *)node->n.left);
    printf("%d color: %lu", node->value, __tree_isset(&node->n, TREE_BLACK));
    if (node->n.left != NIL) printf(" l: %d", ((struct mynode *)node->n.left)->value);
    if (node->n.right != NIL) printf(" r: %d", ((struct mynode *)node->n.right)->value);
    if (!__tree_isset(&node->n, TREE_ROOT)) printf(" p: %d", ((struct mynode *)tree_parent(&node->n))->value);
    printf("\n");
    if (node->n.right != NIL) print((struct mynode *)node->n.right);
}

int check(struct mynode *node) {
    static int last_val;
    if (__tree_isset(&node->n, TREE_ROOT)) last_val = -1;
    if ((void *)node == NIL) return 1;
    assert(node->n.left != &node->n);
    assert(node->n.right != &node->n);
    int l = check((void *)node->n.left);
    assert(node->value >= last_val);
    last_val = node->value;
    int r = check((void *)node->n.right);
    assert(l == r);
    if (!__tree_isset(&node->n, TREE_BLACK)) {
        assert(__tree_isset(node->n.left, TREE_BLACK));
        assert(__tree_isset(node->n.right, TREE_BLACK));
        assert(__tree_isset(tree_parent(&node->n), TREE_BLACK));
    }
    return __tree_isset(&node->n, TREE_BLACK) ? l + 1 : l;
}

static inline int mynode_comp(struct tree_head *a, struct tree_head *b) {
    return ((struct mynode *)a)->value < ((struct mynode *)b)->value;
}

tree_define(mynode, mynode_comp);

int main() {
    struct tree_head *tree = NIL;
    tree_init(&tree);
    struct mynode nodes[10000];
    for (int i = 0; i < 10000; i++) {
        nodes[i].value = rand() % 20000;
        tree_add(&nodes[i].n, &tree, mynode);
        //check((void *)tree);
    }
    //print(tree);
    struct tree_head *node;
    int last_val = -1;
    for_traverse(node, &tree) {
        int val = ((struct mynode *)node)->value;
        printf("%d ", val);
        assert(val >= last_val);
        last_val = val;
    }
    puts("");
    for (int i = 0; i < 10000; i++) {
        if (rand() & 1) tree_del(&nodes[i].n);
        //check((void *)tree);
    }
    last_val = -1;
    for_traverse(node, &tree) {
        int val = ((struct mynode *)node)->value;
        printf("%d ", val);
        assert(val >= last_val);
        last_val = val;
    }
    puts("");
    //print(tree);
    return 0;
}
