/*
 * Copyright (c) 2016 Jianfeng Zhang <swordfeng123@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "rbtree.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef DEBUG
#define assert(cond) do { \
    if (!(cond)) { \
        printf("assert fail: %s, line %d\n", #cond, __LINE__); \
        exit(1); \
    } \
} while (0)
#else
#define assert(cond)
#endif

struct tree_head __tree_nil = { .__tree_i = TREE_BLACK };

static void __tree_rotate_left(struct tree_head *node) {
    struct tree_head *newpa = node->right;
    node->right = newpa->left;
    newpa->left = node;
    __tree_set_parent(newpa, __tree_parent(node));
    __tree_set_parent(node, newpa);
    if (__tree_isset(node, TREE_ROOT)) {
        __tree_unset(node, TREE_ROOT);
        __tree_set(newpa, TREE_ROOT);
        struct tree_head **tree = (struct tree_head **)__tree_parent(newpa);
        *tree = newpa;
    } else {
        struct tree_head *pa = __tree_parent(newpa);
        if (pa->left == node) {
            pa->left = newpa;
        } else {
            pa->right = newpa;
        }
    }
    struct tree_head *mid = node->right;
    if (mid != NIL) {
        __tree_set_parent(mid, node);
    }
}

static void __tree_rotate_right(struct tree_head *node) {
    struct tree_head *newpa = node->left;
    node->left = newpa->right;
    newpa->right = node;
    __tree_set_parent(newpa, __tree_parent(node));
    __tree_set_parent(node, newpa);
    if (__tree_isset(node, TREE_ROOT)) {
        __tree_unset(node, TREE_ROOT);
        __tree_set(newpa, TREE_ROOT);
        struct tree_head **tree = (struct tree_head **)__tree_parent(newpa);
        *tree = newpa;
    } else {
        struct tree_head *pa = __tree_parent(newpa);
        if (pa->right == node) {
            pa->right = newpa;
        } else {
            pa->left = newpa;
        }
    }
    struct tree_head *mid = node->left;
    if (mid != NIL) {
        __tree_set_parent(mid, node);
    }
}

void __tree_add_adjust(struct tree_head *node) {
    assert(!__tree_isset(node, TREE_BLACK));
    assert(__tree_isset(node->left, TREE_BLACK));
    assert(__tree_isset(node->right, TREE_BLACK));
    if (__tree_isset(node, TREE_ROOT)) {
        __tree_set(node, TREE_BLACK);
    } else if (__tree_isset(__tree_parent(node), TREE_BLACK)) {
        // do nothing
    } else {
        struct tree_head *pa = __tree_parent(node);
        assert(!__tree_isset(pa, TREE_ROOT));
        struct tree_head *grandpa = __tree_parent(pa);
        struct tree_head *un = pa == grandpa->left ? grandpa->right : grandpa->left;
        assert(!__tree_isset(pa, TREE_BLACK));
        if (!__tree_isset(un, TREE_BLACK)) {
            __tree_set(pa, TREE_BLACK);
            __tree_set(un, TREE_BLACK);
            __tree_unset(grandpa, TREE_BLACK);
            __tree_add_adjust(grandpa);
        } else if (grandpa->left == pa) {
            assert(__tree_isset(grandpa, TREE_BLACK));
            if (pa->right == node) {
                __tree_rotate_left(pa);
                node = pa;
                pa = __tree_parent(node);
            }
            __tree_unset(grandpa, TREE_BLACK);
            __tree_set(pa, TREE_BLACK);
            __tree_rotate_right(grandpa);
        } else {
            if (pa->left == node) {
                __tree_rotate_right(pa);
                node = pa;
                pa = __tree_parent(node);
            }
            __tree_unset(grandpa, TREE_BLACK);
            __tree_set(pa, TREE_BLACK);
            __tree_rotate_left(grandpa);
        }
    }
}

static void __tree_del_adjust(struct tree_head *node) {
    if (__tree_isset(node, TREE_ROOT)) return;
    struct tree_head *pa = __tree_parent(node);
    struct tree_head *sib = pa->left == node ? pa->right : pa->left;
    if (!__tree_isset(sib, TREE_BLACK)) {
        __tree_unset(pa, TREE_BLACK);
        __tree_set(sib, TREE_BLACK);
        if (pa->left == node) __tree_rotate_left(pa);
        else __tree_rotate_right(pa);
    }
    sib = pa->left == node ? pa->right : pa->left;
    assert(__tree_isset(sib, TREE_BLACK));
    assert(sib != NIL);
    if (__tree_isset(sib->left, TREE_BLACK) && __tree_isset(sib->right, TREE_BLACK)) {
        // 3 or 4
        __tree_unset(sib, TREE_BLACK);
        if (__tree_isset(pa, TREE_BLACK)) {
            // 3
            __tree_del_adjust(pa);
        } else {
            // 4
            __tree_set(pa, TREE_BLACK);
        }
    } else if (node == pa->left) {
        if (__tree_isset(sib->right, TREE_BLACK)) {
            // 5
            __tree_unset(sib, TREE_BLACK);
            __tree_set(sib->left, TREE_BLACK);
            __tree_rotate_right(sib);
            sib = __tree_parent(sib);
        }
        // 6
        __tree_set(sib->right, TREE_BLACK);
        if (!__tree_isset(pa, TREE_BLACK)) __tree_unset(sib, TREE_BLACK);
        __tree_set(pa, TREE_BLACK);
        __tree_rotate_left(pa);
    } else {
        if (__tree_isset(sib->left, TREE_BLACK)) {
            // 5
            __tree_unset(sib, TREE_BLACK);
            __tree_set(sib->right, TREE_BLACK);
            __tree_rotate_left(sib);
            sib = __tree_parent(sib);
        }
        // 6
        __tree_set(sib->left, TREE_BLACK);
        if (!__tree_isset(pa, TREE_BLACK)) __tree_unset(sib, TREE_BLACK);
        __tree_set(pa, TREE_BLACK);
        __tree_rotate_right(pa);
    }
}

static void __tree_replace(struct tree_head *node, struct tree_head *newnode) {
    if (newnode != NIL) {
        newnode->left = node->left;
        newnode->right = node->right;
        newnode->__tree_i = node->__tree_i;
    }
    struct tree_head *pa = __tree_parent(node);
    if (!__tree_isset(node, TREE_ROOT)) {
        if (pa->left == node) {
            pa->left = newnode;
        } else {
            pa->right = newnode;
        }
    } else {
        struct tree_head **tree = (struct tree_head **)pa;
        *tree = newnode;
    }
    if (node->left != NIL) __tree_set_parent(node->left, newnode);
    if (node->right != NIL) __tree_set_parent(node->right, newnode);
}

void __tree_del(struct tree_head *node) {
    if (node->left != NIL && node->right != NIL) {
        struct tree_head *r = node->right;
        while (r->left != NIL) r = r->left;
        __tree_del(r);
        __tree_replace(node, r);
        return;
    }

    struct tree_head *child = node->left == NIL ? node->right : node->left;

    if (__tree_isset(child, TREE_BLACK)) {
        if (__tree_isset(node, TREE_BLACK)) {
            __tree_del_adjust(node);
        } else __tree_set(node, TREE_BLACK);
    }
    if (child != NIL) {
        node->left = child->left;
        node->right = child->right;
    }
    __tree_replace(node, child);
}

struct tree_head *__tree_traverse_init(struct tree_head **tree) {
    struct tree_head *node = *tree;
    while (node->left != NIL) node = node->left;
    return node;
}

struct tree_head *__tree_traverse(struct tree_head *node) {
    if (node == NIL) return NULL;
    if (node->right == NIL) {
        do {
            if (__tree_isset(node, TREE_ROOT)) return NIL;
            struct tree_head *pa = __tree_parent(node);
            if (node == pa->left) return pa;
            node = pa;
        } while (1);
    } else {
        node = node->right;
        while (node->left != NIL) node = node->left;
        return node;
    }
}
