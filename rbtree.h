/*
 * Copyright (c) 2016 Jianfeng Zhang <swordfeng123@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __RBTREE_H__
#define __RBTREE_H__

#include <stdint.h>

struct tree_head {
    struct tree_head *left, *right;
    uintptr_t __tree_i;
};

struct tree_head __tree_nil;
static struct tree_head *const NIL = &__tree_nil;

#include "rbtree.template.h"

#define tree_define(postfix, comparator) \
    __tree_define(postfix, comparator) \
    static inline void tree_add_##postfix(struct tree_head *node, struct tree_head **tree) { \
        __tree_add_##postfix(node, tree); \
    }

#define tree_add(node, tree, postfix) __tree_add_##postfix(node, tree)

static inline void tree_del(struct tree_head *node) {
    __tree_del(node);
}

static inline struct tree_head *tree_parent(struct tree_head *node) {
    return __tree_parent(node);
}

static inline void tree_init(struct tree_head **tree) {
    *tree = NIL;
}

static inline int tree_empty(struct tree_head **tree) {
    return *tree == NIL;
}

#define for_traverse(pos, tree) \
    for (pos = __tree_traverse_init(tree); pos != NIL; pos = __tree_traverse(pos))

#endif // __RBTREE_H__
