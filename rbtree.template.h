/*
 * Copyright (c) 2016 Jianfeng Zhang <swordfeng123@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#define TREE_PARENT_MASK (~3UL)
#define TREE_BLACK 1UL
#define TREE_ROOT 2UL
#define __tree_set(node, x) ((node)->__tree_i |= (x))
#define __tree_unset(node, x) ((node)->__tree_i &= ~(x))
#define __tree_isset(node, x) ((node)->__tree_i & (x))

static inline struct tree_head *__tree_parent(struct tree_head *node) {
    return (struct tree_head *)(node->__tree_i & TREE_PARENT_MASK);
}

static inline void __tree_set_parent(struct tree_head *node, void *parent) {
    node->__tree_i = (node->__tree_i & ~TREE_PARENT_MASK) | (uintptr_t) parent;
}

void __tree_add_adjust(struct tree_head *node);
void __tree_del(struct tree_head *node);
struct tree_head *__tree_traverse_init(struct tree_head **tree);
struct tree_head *__tree_traverse(struct tree_head *node);

#define __tree_define(postfix, comparator) \
\
static void __tree_add_node_##postfix(struct tree_head *newnode, struct tree_head *node) { \
    if (comparator(newnode, node)) { \
        if (node->left == NIL) { \
            node->left = newnode; \
            __tree_set_parent(newnode, node); \
        } else { \
            __tree_add_node_##postfix(newnode, node->left); \
        } \
    } else { \
        if (node->right == NIL) { \
            node->right = newnode; \
            __tree_set_parent(newnode, node); \
        } else { \
            __tree_add_node_##postfix(newnode, node->right); \
        } \
    } \
} \
\
static void __tree_add_##postfix(struct tree_head *node, struct tree_head **tree) { \
    node->__tree_i = 0; \
    __tree_unset(node, TREE_BLACK); \
    __tree_set_parent(node, NIL); \
    node->left = NIL; \
    node->right = NIL; \
    if (*tree == NIL) { \
        *tree = node; \
        __tree_set(node, TREE_ROOT); \
        __tree_set_parent(node, tree); \
    } else { \
        __tree_add_node_##postfix(node, *tree); \
    } \
    __tree_add_adjust(node); \
}
